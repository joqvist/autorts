package org.jastadd.testsel;

import java.io.File;

/**
 * Created by jesper on 2/25/15.
 */
public class PersistentConfiguration {
	private static final String SETTINGS_DIR = ".testsel";
	private static final String SETTINGS_FILE = "settings.cfg";

	private static File settingsDir;
	private static String settingsPath = SETTINGS_FILE;
	static {

		boolean foundSettingsDir = false;
		String userHome = System.getProperty("user.home");
		if (userHome != null && !userHome.isEmpty()) {
			settingsDir = new File(userHome, SETTINGS_DIR);
			if (!settingsDir.exists()) {
				settingsDir.mkdir();
			}
			if (settingsDir.exists() && settingsDir.isDirectory() &&
					settingsDir.canWrite()) {

				settingsPath = new File(settingsDir, SETTINGS_FILE).getAbsolutePath();
				foundSettingsDir = true;
			}
		}

		if (!foundSettingsDir) {
			// no settings directory found
			settingsDir = new File(System.getProperty("user.dir"));
			settingsPath = new File(settingsDir, SETTINGS_FILE).getAbsolutePath();
		}
	}

	/**
	 * @return the default settings directory location
	 */
	public static File settingsDirectory() {
		return settingsDir;
	}

	/**
	 * @return the default settings directory location
	 */
	public static String settingsFilePath() {
		return settingsPath;
	}

}
