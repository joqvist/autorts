package org.jastadd.classpath;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class ClasspathDumper {
	public static void main(String args[]) {
		for (String filename : args) {
			Collection<ClasspathEntry> cp = Collections.emptyList();
			try {
				cp = getClasspath(new File(filename));
			} catch (IOException e) {
				e.printStackTrace();
			}
			boolean first = true;
			for (ClasspathEntry entry : cp) {
				if (entry.isSourcePath()) {
					if (!first) {
						System.out.print(":");
					}
					first = false;
					System.out.print(entry.path);
				}
			}
		}
	}

	/**
	 * Parse the classpath entries in a .classpath file
	 * @param classpathFile The .classpath file to parse
	 * @return The classpath entries
	 * @throws IOException
	 */
	public static Collection<ClasspathEntry> getClasspath(File classpathFile) throws IOException {
		Collection<ClasspathEntry> cp = new ArrayList<ClasspathEntry>();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			SAXHandler handler = new SAXHandler(cp);
			parser.parse(classpathFile, handler);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return cp;
	}

	static class SAXHandler extends DefaultHandler {
		private final Collection<ClasspathEntry> cp;

		public SAXHandler(Collection<ClasspathEntry> cp) {
			this.cp = cp;
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			if (qName.equals("classpathentry")) {
				cp.add(new ClasspathEntry(attributes.getValue("kind"), attributes.getValue("path")));
			}
		}
	}
}
