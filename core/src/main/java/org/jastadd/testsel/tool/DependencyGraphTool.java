package org.jastadd.testsel.tool;

import org.jastadd.testsel.meta.DependencyNode;

import java.util.*;
import java.io.*;

/**
 * A tool that produces a graph of the dependency map.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class DependencyGraphTool {
	
	Map<Integer, Set<Integer>> depends = new HashMap<Integer, Set<Integer>>();
	
	/**
	 * @param root
	 */
	public void createGraphFor(DependencyNode root) {
	
		boolean nodenames = true;
		boolean directed = true;
		
		try {
		
			Set<DependencyNode> allNodes = root.allDependencies();
			allNodes.add(root);
			
			PrintStream out = new PrintStream(new FileOutputStream("test.dot"));

			// build reverse node index
			Map<DependencyNode, Integer> rev = new HashMap<DependencyNode, Integer>();
			int i = 0;
			for (DependencyNode node : allNodes) {
				rev.put(node, i++);
			}
			
			if (directed)
				out.print("di");
			out.println("graph {");

			// node declarations
			for (DependencyNode node : allNodes) {
				out.print("  n" + rev.get(node) + " [");
				if (nodenames) {
					out.print("shape=none,");
					out.print("label=\"" + node.name() + "\"");
				} else {
					out.print("shape=point,");
					out.print("label=\"\"");
				}
				out.println("]");
			}
			
			// add depends
			for (DependencyNode node : allNodes) {
				for (DependencyNode dep : node.directDependencies()) {
					addDepend(out, rev.get(node), rev.get(dep), directed);
				}
			}

			out.println("}");
			out.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Only output the dependency if it was not already present.
	 */
	protected void addDepend(PrintStream out, int a, int b, boolean directed) {
		if (a == b)
			return;
		if (directed) {
			out.println("  n" + a + " -> n" + b);
		} else {
			if (depends.containsKey(a) && depends.get(a).contains(b) ||
					depends.containsKey(b) && depends.get(b).contains(a))
				return;
			out.println("  n" + a + " -- n" + b);
		}
		if (!depends.containsKey(a))
			depends.put(a, new HashSet<Integer>());
		depends.get(a).add(b);
	}
}
