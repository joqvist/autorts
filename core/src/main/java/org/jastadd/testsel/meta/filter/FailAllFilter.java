package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

public class FailAllFilter implements DependencyNodeFilter {

	public static final FailAllFilter INSTANCE = new FailAllFilter();

	private FailAllFilter() {
	}

	@Override
	public boolean pass(DependencyNode node) {
		return false;
	}

}
