package org.jastadd.testsel.meta;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Source package dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production SourcePackage : {@link DependencyNode} ::= {@link SourceFile}* {SourcePackage}*;
 * @ast node
 */
public class SourcePackage extends DependencyNode {

	private final String id;

	/**
	 * Create a new package dependency node.
	 * @param pkgName The full package name
	 */
	public SourcePackage(String pkgName) {
		super(pkgName, pkgName);
		this.id = "package:" + pkgName;
	}

	@Override
	public String id() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean childStateConnected() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean parentStateConnected() {
		return true;
	}

	/**
	 * The dependencies of a package = [childPackage].enclosingPackage(directDependencies(childTypes))
	 */
	@Override
	public Set<DependencyNode> allDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();
		Queue<SourcePackage> worklist = new LinkedList<SourcePackage>();

		worklist.add(this);

		while (!worklist.isEmpty()) {
			for (DependencyNode child : worklist.poll().children) {
				if (child instanceof SourceFile) {
					for (DependencyNode dep : child.directDependencies()) {
						SourcePackage pkg = dep.enclosingPackage();
						if (!depends.contains(pkg)) {
							depends.add(pkg);
							worklist.add(pkg);
						}
					}
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();

		// dependencies through child types
		for (DependencyNode child : children) {
			if (child instanceof SourceFile) {
				for (DependencyNode dep : child.directDependencies()) {
					DependencyNode enclosing = dep.enclosingPackage();
					if (enclosing != null) {
						depends.add(enclosing);
					}
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public boolean hasDependencies() {
		for (DependencyNode child : children) {
			if (child instanceof SourceFile) {
				for (DependencyNode dep : child.allDependencies()) {
					if (dep.enclosingPackage() != this)
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isReflection() {
		return false;
	}

	@Override
	public synchronized Collection<DependencyNode> getDependents() {
		Set<DependencyNode> dependents = new HashSet<DependencyNode>();
		for (DependencyNode child : children) {
			if (child instanceof SourceFile) {
				for (DependencyNode dependent : child.getDependents()) {
					DependencyNode enclosing = dependent.enclosingPackage();
					if (enclosing != null) {
						dependents.add(dependent);
					}
				}
			}
		}
		dependents.remove(this);
		return dependents;
	}

	@Override
	public boolean isPackage() {
		return true;
	}

	@Override
	public synchronized void propagateStateChange() {
		if (state == CacheState.ADDED) {
			for (DependencyNode dependent : reverseDependencies) {
				dependent.setState(CacheState.FLUSHED);
			}
		}

	}

	@Override
	public SourcePackage enclosingPackage() {
		return this;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.SOURCE_PACKAGE;
	}
}
