package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

public class TestFilter implements DependencyNodeFilter {

	public static final TestFilter INSTANCE = new TestFilter();

	private TestFilter() { }

	@Override
	public boolean pass(DependencyNode node) {
		return node.containsTest();
	}

}
