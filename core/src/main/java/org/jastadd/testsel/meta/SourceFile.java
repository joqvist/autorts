package org.jastadd.testsel.meta;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Source file dependency node.
 * A SourceFile must be the child of a SourcePackage.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production SourceFile : {@link DependencyNode} ::= {@link TypeDependency}*;
 * @ast node
 */
public class SourceFile extends DependencyNode {

	private final String path;
	private final String id;
	private long lastModified;

	/**
	 * Create a new file dependency node.
	 * @param path The path name of the source file
	 */
	public SourceFile(String path) {
		this(new File(path), path);
	}

	/**
	 * Create a new file dependency node.
	 * @param file The source file
	 */
	public SourceFile(File file) {
		this(file, file.getPath());
	}

	/**
	 * Create a new file dependency node.
	 * @param file The source file
	 * @param path The source path within the project to the file
	 */
	public SourceFile(File file, String path) {
		super(path, basePathName(path));
		this.path = path;
		this.id = "file:" + path;
		if (file.exists()) {
			this.lastModified = file.lastModified();
		} else {
			this.lastModified = -1;
			setState(CacheState.REMOVED);
		}
	}

	private static String basePathName(String path) {
		String[] parts = path.split("[\\\\/]");
		return parts[parts.length-1];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String id() {
		return id;
	}

	/**
	 * @return The last modified timestamp of the source file
	 */
	public long getLastModified() {
		return lastModified;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean childStateConnected() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean parentStateConnected() {
		return false;
	}

	/**
	 * Source file nodes do not have direct dependencies. This method returns
	 * enclosingSourceFile(allDependencies(children))
	 */
	@Override
	public synchronized Set<DependencyNode> allDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>(directDependencies);

		for (DependencyNode child : children) {
			for (DependencyNode dep : child.allDependencies()) {
				if (!dep.isReflection()) {
					DependencyNode enclosing = dep.enclosingFile();
					if (enclosing != null) {
						depends.add(dep.enclosingFile());
					}
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public synchronized Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>(directDependencies);

		for (DependencyNode child : children) {
			for (DependencyNode dep : child.directDependencies()) {
				if (!dep.isReflection() && dep.enclosingFile() != null) {
					depends.add(dep.enclosingFile());
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public SourceFile enclosingFile() {
		return this;
	}

	@Override
	public boolean hasDependencies() {
		if (!directDependencies.isEmpty()) {
			return true;
		}
		for (DependencyNode child : children) {
			for (DependencyNode dep : child.directDependencies()) {
				if (!dep.isReflection() && dep.enclosingFile() != null
						&& dep.enclosingFile() != this) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isReflection() {
		return false;
	}

	/**
	 * Check the last modified timestamp for the file on disk to see
	 * if the file has been modified since last test refresh.
	 * @return <code>true</code> if the file was modified since last
	 * test refresh
	 */
	public boolean checkTimestamp() {
		File file = new File(path);
		return checkTimestamp(file.lastModified());
	}

	/**
	 * @param timestamp Compare the save timestamp with this value.
	 * @return <code>true</code> if the file was modified
	 */
	public boolean checkTimestamp(long timestamp) {
		if (timestamp != lastModified) {
			setState(CacheState.MODIFIED);
			return true;
		}
		return false;
	}

	/**
	 * Update the last modified timestamp without setting the modified state.
	 * @param file The file object representing the dependency node
	 */
	public void updateTimestamp(File file) {
		setTimestamp(file.lastModified());
	}

	/**
	 * Update the last modified timestamp without setting the modified state.
	 * @param timestamp
	 */
	public void setTimestamp(long timestamp) {
		lastModified = timestamp;
	}

	@Override
	public synchronized Collection<DependencyNode> getDependents() {
		Set<DependencyNode> dependents = new HashSet<DependencyNode>(reverseDependencies);
		for (DependencyNode child : children) {
			for (DependencyNode dependent : child.getDependents()) {
				SourceFile enclosingFile = dependent.enclosingFile();
				if (enclosingFile != null)
					dependents.add(enclosingFile);
			}
		}
		dependents.remove(this);
		return dependents;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.SOURCE_FILE;
	}
}
