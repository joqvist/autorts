package org.jastadd.testsel.meta;

public enum NodeKind {
	GROUP,
	SOURCE_FILE,
	JAR_FILE,
	SOURCE_PACKAGE,
	JAVA_TYPE,
	JAVA_METHOD,
	JAVA_FIELD,
	JAVA_REFLECTION_MEMBER,
}
