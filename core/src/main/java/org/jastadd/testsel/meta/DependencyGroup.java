package org.jastadd.testsel.meta;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * An abstract group of dependency nodes. Can be thought of as grouping
 * related items together, e.g., source files or libraries.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class DependencyGroup extends DependencyNode {

	/**
	 * Qualified name for the libraries group
	 */
	public static final String LIBRARIES_GROUP = "<libraries>";
	public static final String LIBRARIES_ID = "group:<libraries>";
	public static final String JRE_GROUP = "JRE";
	public static final String JRE_ID = "group:JRE";

	private final String id;

	/**
	 * Create a new dependency node group
	 * @param groupName
	 */
	public DependencyGroup(String groupName) {
		super(groupName);
		id = "group:" + groupName;
	}

	@Override
	public String id() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean childStateConnected() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean parentStateConnected() {
		return true;
	}

	/**
	 * Add file to this package
	 * @param file The new child file
	 */
	public final synchronized void addFile(SourceFile file) {
		super.addChild(file);
	}

	/**
	 * Add a child package
	 * @param pkg The new child package
	 */
	public final synchronized void addChildPackage(SourcePackage pkg) {
		super.addChild(pkg);
	}

	/**
	 * The dependencies of a package = [childPackage].enclosingPackage(directDependencies(childTypes))
	 */
	@Override
	public Set<DependencyNode> allDependencies() {
		return Collections.emptySet();
	}

	@Override
	public Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();

		// dependencies through child types
		for (DependencyNode child : children) {
			if (child instanceof SourceFile) {
				for (DependencyNode dep : child.directDependencies()) {
					depends.add(dep.enclosingPackage());
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public boolean hasDependencies() {
		return false;
	}

	@Override
	public boolean isReflection() {
		return false;
	}

	@Override
	public synchronized Collection<DependencyNode> getDependents() {
		return Collections.emptyList();
	}

	@Override
	public boolean isPackage() {
		return true;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.GROUP;
	}

}
