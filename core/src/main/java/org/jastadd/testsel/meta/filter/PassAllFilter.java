package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

public class PassAllFilter implements DependencyNodeFilter {

	public static final PassAllFilter INSTANCE = new PassAllFilter();

	private PassAllFilter() {
	}

	@Override
	public boolean pass(DependencyNode node) {
		return true;
	}

}
