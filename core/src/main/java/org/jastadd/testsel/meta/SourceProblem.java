package org.jastadd.testsel.meta;

import se.llbit.json.JsonObject;
import se.llbit.json.JsonValue;

/**
 * Represents a source code problem in some program node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class SourceProblem {
	private final int line;
	private final int column;
	private final String description;

	public SourceProblem(int line, int column, String description) {
		this.line = line;
		this.column = column;
		this.description = description;
	}

	@Override
	public String toString() {
		return "at " + line + ":" + column + ": " + description;
	}

	public JsonValue toJson() {
		JsonObject obj = new JsonObject();
		obj.add("line", line);
		obj.add("column", column);
		obj.add("description", description);
		return obj;
	}

	public static SourceProblem fromJson(JsonObject obj) {
		int line = obj.get("line").intValue(0);
		int column = obj.get("column").intValue(0);
		String desc = obj.get("description").stringValue("");
		return new SourceProblem(line, column, desc);
	}
}
