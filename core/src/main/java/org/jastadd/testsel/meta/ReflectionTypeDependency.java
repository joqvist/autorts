package org.jastadd.testsel.meta;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Reflection dependency node.
 * A ReflectionDependency must be connected to a DependencyMap.
 * The reflection dependency uses the dependency map to calculate
 * it's dependencies.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production ReflectionTypeDependency : {@link TypeDependency};
 * @ast node
 */
public class ReflectionTypeDependency extends TypeDependency {

	/**
	 * The qualified name of this node.
	 */
	public static final String ID = "reflection:type";
	private final DependencyMap map;

	/**
	 * Create a new reflection type dependency node.
	 * @param map The dependency map that this reflection dependency will
	 * be a part of.
	 */
	public ReflectionTypeDependency(DependencyMap map) {
		super(ID);
		this.map = map;
	}

	@Override
	public String id() {
		return ID;
	}

	@Override
	public boolean hasDependencies() {
		return map.hasTypeDependencies();
	}

	@Override
	public Iterator<DependencyNode> dependencyIterator() {
		return map.typeIterator();
	}

	@Override
	public Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();
		Iterator<DependencyNode> iter = map.typeIterator();
		while (iter.hasNext()) {
			depends.add(iter.next());
		}
		return depends;
	}

	@Override
	public int numDependencies() {
		return map.numTypeDependencies();
	}

	@Override
	public boolean isReflection() {
		return true;
	}
}
