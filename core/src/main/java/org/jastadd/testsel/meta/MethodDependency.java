package org.jastadd.testsel.meta;


/**
 * Method dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production MethodDependency : {@link MemberDependency};
 * @ast node
 */
public class MethodDependency extends MemberDependency {

	/**
	 * Construct a new dependency node for a method with the given name.
	 * @param qualifiedName
	 */
	public MethodDependency(String qualifiedName) {
		super(qualifiedName);
	}

	@Override
	public boolean isMethod() {
		return true;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.JAVA_METHOD;
	}
}
