package org.jastadd.testsel.util;

import java.util.LinkedList;
import java.util.List;

import org.jastadd.testsel.TestSelector.SaveOption;

/**
 * Utility methods to manage options for Dependency Viewer.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class DependencyViewerOptions {
	/**
	 * @param save
	 * @return <code>true</code> if the incremental save option is enabled
	 */
	public static boolean getIncrementalSave(SaveOption save) {
		return save != SaveOption.DO_NOT_SAVE && (
				save == SaveOption.FORCE_SAVE ||
				save == SaveOption.USE_CONFIG && getIncrementalSave());
	}
	/**
	 * @param save
	 * @return <code>true</code> if the incremental save option is enabled
	 */
	public static boolean getIncrementalSave() {
		return ProgramProperties.getBooleanProperty("incrementalSave", true);
	}
	/**
	 * The poll filesystem option controls whether or not periodic polling
	 * of the file system is enable.d
	 * @return <code>true</code> if the incremental update option is enabled
	 */
	public static boolean getPollFileSystem() {
		return ProgramProperties.getBooleanProperty("pollFileSystem", true);
	}

	/**
	 * @param value
	 */
	public static void setIncrementalSave(boolean value) {
		ProgramProperties.setProperty("incrementalSave", ""+value);
	}

	/**
	 * @param value
	 */
	public static void setPollFileSystem(boolean value) {
		ProgramProperties.setProperty("pollFileSystem", ""+value);
	}

	/**
	 * @return <code>true</code> if the automatic dependency refresh option is enabled
	 */
	public static boolean getAutomatedDependencyRefresh() {
		return ProgramProperties.getBooleanProperty("automatedDependencyRefresh", true);
	}

	/**
	 * Update the automated dependency refresh option
	 * @param value
	 */
	public static void setAutomatedDependencyRefresh(boolean value) {
		ProgramProperties.setProperty("automatedDependencyRefresh", value);
	}

	public static boolean hideGhostFiles() {
		return ProgramProperties.getBooleanProperty("hideGhostFiles", true);
	}

	public static void setHideGhostFiles(boolean value) {
		ProgramProperties.setProperty("hideGhostFiles", value);
	}

	public static boolean hideLibraryNodes() {
		return ProgramProperties.getBooleanProperty("hideLibraryNodes", true);
	}

	public static void setHideLibraryNodes(boolean value) {
		ProgramProperties.setProperty("hideLibraryNodes", value);
	}

	public static String getEditorCommand() {
		String cmd = ProgramProperties.getProperty("editorCommand", "");
		if (cmd.isEmpty()) {
			return defaultTextEditor();
		} else {
			return cmd;
		}
	}

	/**
	 * @return the platform-dependent default text editor.
	 */
	private static String defaultTextEditor() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.startsWith("win")) {
			return "notepad";
		} else if (os.startsWith("mac")) {
			return "TextEdit";
		} else {
			return "gedit";
		}
	}

	public static void setEditorCommand(String command) {
		ProgramProperties.setProperty("editorCommand", command);
	}

	public static List<String> getRecentProjects() {
		List<String> projects = new LinkedList<String>();
		String recentProjects = ProgramProperties.getProperty("recentProjects", "");
		for (String project: recentProjects.split(":")) {
			if (!project.isEmpty()) {
				projects.add(project);
			}
		}
		return projects;
	}

	public static void addRecentProject(String projectPath) {
		List<String> projects = getRecentProjects();
		if (!projects.contains(projectPath)) {
			projects.add(0, projectPath);
			while (projects.size() > 5) {
				projects.remove(5);
			}

		}
		StringBuilder property = new StringBuilder();
		boolean first = true;
		for (String project: projects) {
			if (!first) property.append(':');
			first = false;
			property.append(project);
		}
		ProgramProperties.setProperty("recentProjects", property.toString());
	}

}
