package org.jastadd.testsel.util;

/**
 * Java doesn't have a Pair class.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 *
 * @param <U>
 * @param <V>
 */
public class Tuple<U, V> {
	/**
	 * Thing one
	 */
	public U thing1;
	
	/**
	 * Thing two
	 */
	public V thing2;
	
	/**
	 * Create a new tuple of two things.
	 * @param one Thing one
	 * @param two Thing two
	 */
	public Tuple(U one, V two) {
		thing1 = one;
		thing2 = two;
	}
}
