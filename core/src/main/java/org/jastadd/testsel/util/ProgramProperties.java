package org.jastadd.testsel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jastadd.log.Log;
import org.jastadd.testsel.PersistentConfiguration;

/**
 * Utility class for managing program properties.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public final class ProgramProperties {

	private static Properties properties = new Properties();
	private static boolean modified = false;
	private static Map<String, URL> jarMap = null;
	private static final String settingsFilePath;
	private static final File settingsDir;

	private ProgramProperties() { }

	static {
		settingsDir = PersistentConfiguration.settingsDirectory();
		settingsFilePath = PersistentConfiguration.settingsFilePath();
		loadProperties();
	}

	/**
	 * Load saved properties.
	 */
	private static synchronized void loadProperties() {
		try {
			InputStream in = new FileInputStream(settingsFilePath);
			properties.load(in);
//			Log.info("Properties loaded from " + settingsFilePath);
		} catch (IOException e) {
			Log.warn("Could not load the property  file " + settingsFilePath +
					" - defaults will be used");
		}
	}

	/**
	 * Save settings to file.
	 * @param comment Comment
	 */
	private static synchronized void saveProperties(String comment) {
		if (modified) {
			try {
				OutputStream out = new FileOutputStream(settingsFilePath);
				properties.store(out, comment);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Get a property value
	 * @param name Property name
	 * @param defValue Default value
	 * @return The value of the property, or <code>defValue</code> if
	 * the property was not set.
	 */
	public static synchronized String getProperty(String name, String defValue) {
		return properties.getProperty(name, defValue);
	}

	/**
	 * Get the integer value of a property
	 * @param name Property name
	 * @param defValue Default value
	 * @return The value of the property, or <code>defValue</code> if
	 * the property was not set, or was not set to a valid integer value.
	 */
	public static synchronized int getIntProperty(String name, int defValue) {
		try {
			return Integer.parseInt(properties.getProperty(name, ""+defValue));
		} catch (NumberFormatException e) {
			return defValue;
		}
	}

	/**
	 * Get the boolean value of a property
	 * @param name Property name
	 * @param defValue Default value
	 * @return The value of the property, or <code>defValue</code> if
	 * the property was not set, or was not set to a valid integer value.
	 */
	public static synchronized boolean getBooleanProperty(String name, boolean defValue) {
		try {
			return Boolean.parseBoolean(properties.getProperty(name, ""+defValue));
		} catch (NumberFormatException e) {
			return defValue;
		}
	}

	public static synchronized void setProperty(String name, boolean value) {
		setProperty(name, ""+value);
	}

	/**
	 * Set the value of a property
	 * @param name Property name
	 * @param value Property value
	 */
	public static synchronized void setProperty(String name, String value) {
		// only set the property if the value has changed
		if ((value == null && properties.getProperty(name) != null)
				|| (value != null && !value.equals(properties.getProperty(name)))) {

			modified = true;
			properties.setProperty(name, value);
			saveProperties("Dependency Viewer settings");
		}
	}

	/**
	 * Get a propertie's value
	 * @param name Property name
	 * @return The propertie's value, or <code>null</code> if the
	 * property was not set.
	 */
	public static synchronized String getProperty(String name) {
		return properties.getProperty(name);
	}

	/**
	 * @param key property key
	 * @return <code>true</code> if there is a property value for the given key
	 */
	public static synchronized boolean containsKey(String key) {
		return properties.containsKey(key);
	}

	/**
	 * Get unpacked jar file or fall back on class loader to find the resource
	 * @param name Jar path in the Chunky jar file
	 * @return URL to the unpacked jar or <code>null</code> if no such jar was
	 * unpacked
	 */
	public static File getJarFile(String name) {
		URL url = getJarURL(name);
		if (url != null) {
			try {
				return new File(url.toURI());
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		// last resort
		return new File(name);
	}

	/**
	 * Get unpacked jar file or fall back on class loader to find the resource
	 * @param name Jar path in the Chunky jar file
	 * @return URL to the unpacked jar or <code>null</code> if no such jar was
	 * unpacked
	 */
	public static URL getJarURL(String name) {
		initJarMap();
		URL url = jarMap.get(name);
		if (url != null) {
			return url;
		} else {
			ClassLoader parentCL = ProgramProperties.class.getClassLoader();
			return parentCL.getResource(name);
		}
	}

	private static void initJarMap() {
		synchronized (ProgramProperties.class) {
			if (jarMap == null) {
				jarMap = new HashMap<String, URL>();
				addJarFiles("lib");
				addJarFiles("tools");
			}
		}
	}

	private static void addJarFiles(String name) {
		File[] fileList = new File(settingsDir, name).listFiles();
		if (fileList != null) {
			for (File lib: fileList) {
				if (lib.getName().endsWith(".jar")) {
					try {
						jarMap.put(name + "/" + lib.getName(), lib.toURI().toURL());
					} catch (MalformedURLException e) {
					}
				}
			}
		}
	}
}
