package org.jastadd.testsel.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for calculating relative file paths.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * 
 */
public class RelativePath {

	private static final String SEPARATOR = "/";

	private static final List<String> cwd;
	
	static {
		File dir = new File(System.getProperty("user.dir"));

		if (dir.isDirectory()) {
			cwd = buildPathList(dir);
		} else {
			cwd = new LinkedList<String>();
			System.err.println(
					"Error: Could not find current working directory");
		}
	}

	/**
	 * Builds the path list used to compute relative paths
	 * @param path
	 * @return The path list for the given path
	 */
	public static List<String> buildPathList(File path) {
		List<String> parts = new ArrayList<String>();

		try {
			path = path.getCanonicalFile();
			while (path != null) {
				parts.add(0, path.getName());
				path = path.getParentFile();
			}
		} catch (IOException e) {
			return new LinkedList<String>();
		}

		return parts;
	}

	/**
	 * Returns a string representation of the relative path for
	 * the path <code>path</code> from the current working directory.
	 * @param path The original path we want a relative path for
	 * @return The string representing the relative path that corresponds
	 * to the absolute path
	 */
	public static String getRelativePath(String path) {
		return getRelativePath(path, cwd);
	}
	
	/**
	 * Compute the relative path to a file from a base path
	 * @param path
	 * @param base The path list for the base path
	 * @return The relative path to the file at a given path
	 */
	public static String getRelativePath(String path, List<String> base) {
		StringBuilder str = new StringBuilder();
		List<String> leaf = buildPathList(new File(path));

		int I = base.size();
		int J = leaf.size();
		int n = 0;

		while (n < I && n < J && base.get(n).equals(leaf.get(n))) {
			n += 1;
		}

		int o = n;
		while (o < I) {
			str.append(".." + SEPARATOR);
			o += 1;
		}

		while (n < J) {
			str.append(leaf.get(n));

			n += 1;
			if (n < J) {
				str.append(SEPARATOR);
			}
		}
		return str.toString();
	}
}
