package org.jastadd.testsel.testapi;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

import org.jastadd.log.Log;
import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.util.ProgramProperties;

/**
 * Runs the tests with JUnit. The tests are assumed to be compiled in the "bin" directory.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JUnitTestRunner implements TestRunner {

	private final List<TestListener> listeners = new LinkedList<TestListener>();

	private final String testRunnerClassname;

	protected JUnitTestRunner(String classname) {
		testRunnerClassname = classname;
	}

	@Override
	public boolean runTests(Collection<DependencyNode> testNodes,
			DependencyMap map, int numTests) {

		// create a new class loader to load the test classes with
		List<String> testClassPath = new ArrayList<String>();
		Collection<File> jarFiles = map.getJarLibraries();
		for (File jar: jarFiles) {
			testClassPath.add(jar.getPath());
		}

		URL testapiExternal = ProgramProperties.getJarURL(
				"tools/testapi-external.jar");
		if (testapiExternal == null) {
			Log.error("Could not load external test api - can not run tests!\n" +
					"The required file tools/testapi-external.jar seems to be missing!");
			return false;
		}

		URL[] urls = new URL[testClassPath.size() + 2];
		for (int i = 0; i < testClassPath.size(); ++i) {
			String path = testClassPath.get(i);
			try {
				urls[i] = new File(path).toURI().toURL();
			} catch (MalformedURLException e) {
				Log.error(e);
			}
		}
		urls[urls.length-2] = map.getOutputDir();
		urls[urls.length-1] = testapiExternal;
		ClassLoader classloader = new URLClassLoader(urls,
			this.getClass().getClassLoader());

		// build list of classes to test
		Collection<String> tests = new LinkedList<String>();
		for (DependencyNode test: testNodes) {
			tests.add(test.fullName());
		}

		TestThread testThread = new TestThread(tests);
		testThread.setContextClassLoader(classloader);
		testThread.start();
		try {
			testThread.join();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return testThread.wasSuccessful();
	}

	class TestThread extends Thread {
		private boolean successful = false;
		private final Collection<String> tests;

		TestThread(Collection<String> tests) {
			super("test runner");
			this.tests = tests;
		}

		public boolean wasSuccessful() {
			return successful;
		}

		@Override
		public void run() {
			try {
				Class<?> junitRunnerClass = getContextClassLoader().loadClass(
						testRunnerClassname);

				ExternalTestRunner runner = (ExternalTestRunner) junitRunnerClass.newInstance();
				for (TestListener listener: listeners) {
					runner.addListener(listener);
				}
				successful = runner.runTests(tests);
			} catch (InstantiationException e) {
				Log.error("Failed to run unit tests", e);
				failAll(tests);
			} catch (IllegalAccessException e) {
				Log.error("Failed to run unit tests", e);
				failAll(tests);
			} catch (ClassNotFoundException e) {
				Log.error("Failed to run unit tests: " +
						"Class loading error using parent class loader " +
					this.getClass().getClassLoader().getClass().getName(), e);
				failAll(tests);
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				failAll(tests);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				failAll(tests);
			} catch (Throwable e) {
				Log.warning("Unhandled exception during test running", e);
				failAll(tests);
			} finally {
				// TODO this does not work, find some working way to do this!!
				/*// find lingering thread pool workers and interrupt them
				Set<Thread> threads = Thread.getAllStackTraces().keySet();
				for (Thread thread: threads) {
					if (thread.getName().matches("pool-\\d+-thread-\\d+")) {
						thread.interrupt();
					}
				}*/
			}
		}

		private void failAll(Collection<String> tests) {
			for (TestListener listener: listeners) {
				for (String test: tests) {
					listener.testError(test, "test suite exception");
				}
			}
		}
	}

	@Override
	public final void addListener(TestListener listener) {
		listeners.add(listener);
	}

	@Override
	public final void removeListener(TestListener listener) {
		listeners.remove(listener);
	}

}
