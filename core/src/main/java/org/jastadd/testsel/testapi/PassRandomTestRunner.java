package org.jastadd.testsel.testapi;

import java.util.Random;

import org.jastadd.testsel.meta.TestResult;

/**
 * A test runner that randomly passes/fails tests
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class PassRandomTestRunner extends MockTestRunner {

	private final Random random;

	/**
	 * Construct a random-pass test runner
	 */
	public PassRandomTestRunner() {
		random = new Random(System.currentTimeMillis());
	}

	@Override
	protected TestResult getTestResult(String testName) {
		return random.nextBoolean() ? TestResult.PASSED : TestResult.FAILED;
	}
}
