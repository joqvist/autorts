package org.jastadd.testsel.storage;

import java.io.File;
import java.io.IOException;

import org.jastadd.testsel.meta.DependencyMap;

/**
 * Interface for map file formats.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface MapFileFormat {

	MapFormat DEFAULT_FORMAT = MapFormat.JSON;

	static final int FLAG_GHOST_FILE = 0x08;
	static final int FLAG_USER_TEST = 0x04;
	static final int FLAG_SAFE = 0x02;
	static final int FLAG_TEST = 0x01;

	/**
	 * Load a dependency map from disk.
	 *
	 * @param map The dependency map to fill in with the loaded dependencies
	 * @param mapFile The dependency map file to load
	 * @throws IOException
	 */
	public void readMap(DependencyMap map, File mapFile) throws IOException;

	/**
	 * Save a dependency map to disk.
	 *
	 * @param map The dependency map to save
	 * @param mapFile The file to save the dependency map in
	 * @throws IOException
	 */
	public void writeMap(DependencyMap map, File mapFile) throws IOException;
}
