package org.jastadd.testsel.storage;

import java.io.IOException;

/**
 * Indicates a format error in the map file.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
@SuppressWarnings("serial")
public class DependencyMapIOError extends IOException {

	/**
	 * @param message
	 */
	public DependencyMapIOError(String message) {
		super(message);
	}

}
