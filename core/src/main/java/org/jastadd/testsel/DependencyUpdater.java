package org.jastadd.testsel;

import org.jastadd.testsel.TestSelector.SaveOption;
import org.jastadd.testsel.util.DependencyViewerOptions;

/**
 * Periodically refreshes dependencies
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class DependencyUpdater extends Thread {

	private final TestSelector map;
	private boolean forceRefresh = false;

	/**
	 * Create a new dependency refresher
	 * @param viewer
	 */
	public DependencyUpdater(TestSelector viewer) {
		super("Dependency Updater");
		this.map = viewer;
	}

	@Override
	public void run() {
		try {
			while (!isInterrupted()) {
				sleep(750);

				if (checkForceRefresh() ||
						DependencyViewerOptions.getAutomatedDependencyRefresh() &&
						map.needsRefresh()) {
					map.parseDependencies(SaveOption.USE_CONFIG);
				}
			}
		} catch (InterruptedException e) {
		}
	}

	public synchronized void forceRefresh() {
		forceRefresh = true;
	}

	private synchronized boolean checkForceRefresh() {
		boolean result = forceRefresh;
		forceRefresh = false;
		return result;
	}
}
