package org.jastadd.testsel;

import org.jastadd.testsel.project.Project;

public interface ProgressListener {

  void setProgress(String string, int i, int j, int target);

  void resetProgress();

  // progress.setReflectionNodes(reflectionDependentNodes());
  void updateReflectionNodes();

  void projectLoaded(Project newProject);

  void updateTestResult(String testName);

  /**
   * Updates the cache status of all test nodes before a test run.
   */
  void beforeTestRun();

  void projectConfigMissing();

  void info(String message);

  void synchronizeMapBlocking();

  void synchronizeMapNonBlocking();

}
