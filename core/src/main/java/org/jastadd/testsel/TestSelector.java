package org.jastadd.testsel;

import org.extendj.ast.CompilationUnit;
import org.extendj.ast.JarFilePath;
import org.extendj.ast.Options;
import org.extendj.ast.Program;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jastadd.classpath.ClasspathDumper;
import org.jastadd.classpath.ClasspathEntry;
import org.jastadd.log.Log;
import org.jastadd.testsel.meta.*;
import org.jastadd.testsel.meta.filter.TestFilter;
import org.jastadd.testsel.project.Project;
import org.jastadd.testsel.testapi.TestListener;
import org.jastadd.testsel.testapi.TestRunner;
import org.jastadd.testsel.testapi.TestRunner.Runner;
import org.jastadd.testsel.testapi.TestRunnerFactory;
import org.jastadd.testsel.tool.DependencyTool;
import org.jastadd.testsel.util.DependencyViewerOptions;
import org.jastadd.testsel.util.ProgramProperties;
import org.jastadd.testsel.util.RelativePath;
import org.jastadd.util.RobustMap;

/**
 * This is the main test runner and dependency tracker
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class TestSelector extends DependencyTool {

  private boolean needsRefresh = false;

  ProgressListener progress = new SilentProgressListener();

  protected FileSystemPoller fileSystemPoller = new FileSystemPoller(this);

  public TestSelector(Project project) {
    this.project = project;
  }

  public void setProgressListener(ProgressListener listener) {
    progress = listener;
  }

  /**
   * Clean up and shut down test selector.
   */
  public void dispose() {
  }

  protected void initProject(String projectPath) {
    Project newProject;
    if (!projectPath.isEmpty()) {
      if (!new File(projectPath).isDirectory()) {
        Log.error("Could not open project directory: " + projectPath);
        System.exit(1);
        return;
      }
      if (projectPath.isEmpty()) {
        Log.error("Can not create project with empty base directory path");
        System.exit(1);
        return;
      } else {
        String projectName = new File(projectPath).getName();
        newProject = new Project(projectName, projectPath);
      }
    } else {
      newProject = Project.openPreviousProject();
      if (newProject == Project.DEFAULT_PROJECT) {
        projectPath = ProgramProperties.getProperty("projectPath", "");
        if (!projectPath.isEmpty() && new File(projectPath).isDirectory()) {
          if (projectPath.isEmpty()) {
            Log.error("Can not create project with empty base directory path");
            System.exit(1);
            return;
          } else {
            String projectName = new File(projectPath).getName();
            newProject = new Project(projectName, projectPath);
          }
        }
      }
    }
    //Log.info("Opening project " + newProject.getBaseDir().getPath());
    openProject(newProject);
  }

  public enum SaveOption {
    DO_NOT_SAVE,
    FORCE_SAVE,
    USE_CONFIG,
  }

  /**
   * Refresh the dependencies for the program.
   * Attempts to perform an iterative dependency refresh if possible.
   */
  public synchronized void parseDependencies(SaveOption save) {

    needsRefresh = false;

    // Incremental parse.
    long parseStart = System.currentTimeMillis();

    // Find new additions to the classpath.
    checkSourceSet();

    Collection<DependencyNode> worklist = new LinkedList<DependencyNode>();
    Collection<DependencyNode> missing = new HashSet<DependencyNode>(sourceFiles());
    Collection<File> sourceFiles = getSourceFiles();

    for (File sourceFile : sourceFiles) {

      String path = project.getRelativePath(sourceFile.getPath());
      SourceFile node = reversePathMap.get(path);

      if (node != null) {
        if (node.mustParse()) {
          // Node was modified or added.
          worklist.add(node);
        }
        missing.remove(node);
      } else {
        DependencyNode added = lookup("file:" + path);
        added.setState(CacheState.ADDED);
        worklist.add(added);
      }
    }

    // For each package check if any file was added/removed,
    // and if so add all files in the package to the worklist.
    for (DependencyNode pkg: packages()) {
      boolean packageModified = false;
      Iterator<DependencyNode> iter = pkg.childIterator();
      while (iter.hasNext()) {
        DependencyNode file = iter.next();
        CacheState state = file.getState();
        if (state == CacheState.ADDED || state == CacheState.REMOVED) {
          packageModified = true;
          break;
        }
      }
      if (packageModified) {
        iter = pkg.childIterator();
        while (iter.hasNext()) {
          DependencyNode file = iter.next();
          if (!file.mustParse()) {
            worklist.add(file);
          }
        }
      }
    }

    // Remove all dependency edges from the modified and removed files
    // to the rest of the dependency map.

    for (DependencyNode node: worklist) {
      node.clear();
    }

    for (DependencyNode node : missing) {
      Log.info("File deleted: " + node.fullName());
      node.removeFromMap(this);
    }

    long modCheckTime = System.currentTimeMillis() - parseStart;
    Log.info(String.format("Modification checking took %d millis",
        modCheckTime));

    // Skip program node construction and parsing if worklist is empty.
    if (!worklist.isEmpty()) {
      try {
        Program program = initProgram();
        int target = program.getNumCompilationUnit();
        progress.setProgress("Refreshing dependencies", 0, 0, target);

        int parsed = 0;

        // Add the initial worklist items to the parse queue.
        for (DependencyNode node: worklist) {
          String path = node.fullName();
          try {
            program.addSourceFile(project.getLocalFile(path).getPath());
          } catch (UnsupportedOperationException e) {
            System.err.println("problem while adding source path " + path);
            e.printStackTrace();
          }
        }

        Iterator<CompilationUnit> iter = program.compilationUnitIterator();
        while (iter.hasNext()) {
          CompilationUnit unit = iter.next();

          int done = ++parsed;
          target = program.getNumCompilationUnit();
          progress.setProgress("Refreshing dependencies", done, 0, target);

          try {
            unit.fillInDependencies(this);
          } catch (Throwable t) {
            System.out.println("Error while analyzing " + unit.errorPrefix());
            throw t;
          }
        }

        Log.info(String.format("Parsed %d java files", program.numJavaFiles));
        Log.info(String.format("Parsing took %d millis", program.javaParseTime / 1000000));
      } catch (Throwable e) {
        Log.error("Unexpected exception raised during dependency analysis", e);
      }
    }

    long parseTime = System.currentTimeMillis() - parseStart;
    Log.info(String.format("Dependency refresh took %d millis", parseTime));

    progress.resetProgress();

    propagateNodeStates();

    if (DependencyViewerOptions.getIncrementalSave(save)) {
      // Perform incremental map save.
      saveProject();
    }

    progress.synchronizeMapNonBlocking();
    progress.updateReflectionNodes();
  }

  private List<String> basePath = Collections.emptyList();

  /**
   * Synchronize the cache status of the dependency nodes with the
   * source files on the filesystem.
   */
  private void checkSourceSet() {
    List<String> basePath = RelativePath.buildPathList(project.getBaseDir());

    // Check for new files on the classpath.
    for (ClasspathEntry entry : classpath) {
      //File file = new File(project.getBaseDir(), entry);
      File file = entry.getFile(project);
      if (file.isDirectory()) {
        checkSourceDirectory(file, "", basePath);
      } else if (file.isFile()) {
        checkSourceFile(file, "", basePath);
      }
    }
  }

  private void checkSourceDirectory(File dir, String pkgName,
      List<String> basePath) {
    File[] files = dir.listFiles();
    FileFilter filter = sourceFileFilter();

    for (File file : files) {
      if (filter.accept(file)) {
        if (file.isDirectory()) {
          checkSourceDirectory(
              file,
              pkgName.isEmpty() ? file.getName() : pkgName + "." + file.getName(),
              basePath);
        } else {
          checkSourceFile(file, pkgName, basePath);
        }
      }
    }
  }

  /**
   * Check source file for modification.
   */
  private void checkSourceFile(File file, String pkgName,
      List<String> basePath) {
    String path = RelativePath.getRelativePath(file.getPath(), basePath);
    String id = "file:" + path;
    SourceFile node = (SourceFile) map.get(id);
    if (node == null) {
      boolean isJar = path.endsWith(".jar");
      if (!isJar) {
        Log.info("File added: " + path);
      }

      // Add the source file to the dependency map.
      DependencyNode sourceFile = lookup(id);
      sourceFile.setState(CacheState.ADDED);
    } else {
      long modTime = file.lastModified();
      boolean modified = node.checkTimestamp(modTime);
      if (modified) {
        Log.info("File modified: " + path);
        node.setTimestamp(modTime);
      }
    }
  }

  /**
   * Add jar libraries
   * @param program
   */
  private synchronized void addJarLibraries(Program program) {
    String[] bootclasspaths;
    if (program.options().hasValueForOption("-bootclasspath")) {
      bootclasspaths = program.options().getValueForOption("-bootclasspath").split(File.pathSeparator);
    } else {
      bootclasspaths = System.getProperty("sun.boot.class.path").split(File.pathSeparator);
    }
    DependencyGroup jre = jreGroup();
    librariesGroup().addChild(jre);
    for (String jar: bootclasspaths) {
      addExternalJarLibrary(program, new File(jar), jre);
    }
    for (ClasspathEntry entry: classpath) {
      if (entry.isLibrary()) {
        addJarLibrary(program, entry.getFile(project));
      } else if (entry.isContainer()) {
        DependencyNode group = lookup("group:" + entry.toString());
        librariesGroup().addChild(group);
        for (File jarFile: entry.getContainerFiles()) {
          addExternalJarLibrary(program, jarFile, group);
        }
      }
    }
  }

  /**
   * Add a jar file to the program source path
   * @param program
   * @param jarFile
   */
  private void addJarLibrary(Program program, File jarFile) {
    try {
      JarFilePath jar = new JarFilePath(jarFile);
      program.addClassPath(jar);
      String relativePath = project.getRelativePath(jar.getPath());
      DependencyNode jarDep = lookup("file:" + relativePath);
      librariesGroup().addChild(jarDep);
    } catch (IOException e) {
    }
  }

  /**
   * Add an external jar file to the program source path
   * @param program
   * @param jarFile
   * @param group
   */
  private void addExternalJarLibrary(Program program, File jarFile, DependencyNode group) {
    try {
      JarFilePath jar = new JarFilePath(jarFile);
      program.addClassPath(jar);
      DependencyNode jarDep = lookup("file:" + jarFile.getAbsolutePath());
      group.addChild(jarDep);
    } catch (IOException e) {
    }
  }

  private void propagateUnknownTestResults() {
    for (DependencyNode node : map.values()) {
      if (node.isTest() && node.isFlushed()) {
        node.setTestResult(TestResult.UNKNOWN);
      }
    }
  }

  /**
   * Reset all test results.
   */
  public void clearTestResults() {
    for (DependencyNode node : map.values()) {
      if (node.isTest()) {
        node.setTestResult(TestResult.UNKNOWN);
      }
    }
  }

  /**
   * Load the project's map file
   * @return <code>true</code> if the map was successfully loaded
   */
  public synchronized boolean loadProject() {

    if (project == Project.DEFAULT_PROJECT) {
      Log.info("Can not load map file: project not set");
      return false;
    }

    progress.setProgress("Loading project", 0, 0, 1);
    try {
      if (project.getMapFile().isFile()) {
        loadMap(project.getMapFile());
      } else {
        resetDependencyMap();
      }
      loadProject(project);

      notifyClasspathChanged(this);
      propagateNodeStates();
      progress.updateReflectionNodes();
      return true;

    } catch (IOException e) {
      Log.warn("Error while loading dependency map", e);
      return false;
    } finally {
      progress.resetProgress();
    }
  }

  public synchronized void propagateNodeStates() {
    for (DependencyNode node: map.values()) {
      if (!node.isCached()) {
        node.propagateStateChange();
      }
    }
    updateFlushedReflectionNodes();
  }

  /**
   * Save the dependency map to a file
   */
  public synchronized void saveProject() {
    if (project == Project.DEFAULT_PROJECT) {
      Log.warn("Can not save map file: project not set");
    } else {
      progress.setProgress("Saving project", 0, 0, 1);
      try {
        saveProject(project);
        saveMap(project.getMapFile());
      } catch (IOException e) {
        Log.error("Error while saving dependency map", e);
      } finally {
        progress.resetProgress();
      }
    }
  }

  /**
   * Parse classpath entries from the current project's .classpath file
   */
  public void loadClasspath() {
    if (project == Project.DEFAULT_PROJECT) {
      Log.info("Can not load .classpath file: no project set");
      return;
    } else {
      loadClasspath(project.getClasspathFile());
    }
  }

  // TODO: make synchronized?
  /**
   * Parse classpath entries from a .classpath file
   * @param classpathFile
   */
  private void loadClasspath(File classpathFile) {
    try {
      Collection<ClasspathEntry> newClasspath =
          ClasspathDumper.getClasspath(classpathFile);

      classpath = newClasspath;

      // tell UI to update
      notifyClasspathChanged(this);
    } catch (FileNotFoundException e) {
      Log.warn("Could not find .classpath file!");
    } catch (IOException e) {
      Log.warn("Failed to read .classpath file!");
    }
  }

  /**
   * Mark every file as cached. This will reset the modified and cached files
   * to the cached state.
   */
  public synchronized void clearFlushedStatus() {
    for (DependencyNode node : map.values()) {
      node.clearFlushed();
    }
  }

  /**
   * Update timestamps of source files
   */
  public synchronized void updateTimestamps() {
    for (SourceFile sourceFile : reversePathMap.values()) {
      sourceFile.updateTimestamp(project.getLocalFile(sourceFile.id()));
    }
  }

  /**
   * Called when the dependency map has been updated and the
   * UI needs to be updated accordingly.
   */
  public void synchronizeViewBlocking() {
    progress.synchronizeMapBlocking();
  }

  /**
   * Lookup dependency node for a file in the default package.
   * @param file
   * @return The dependency node for the given file
   */
  public SourceFile lookupSourceFile(File file) {
    SourceFile sourceFile = lookupSourceFile(file);
    defaultPackage().addChild(sourceFile);
    return sourceFile;
  }

  /**
   * Flush reflection nodes that depend on flushed nodes.
   */
  private void updateFlushedReflectionNodes() {
    Iterator<DependencyNode> iter = reflectionType().dependencyIterator();
    while (iter.hasNext()) {
      if (iter.next().isFlushed()) {
        reflectionType().setState(CacheState.FLUSHED);
        break;
      }
    }
  }

  /**
   * Select tests to run.
   *
   * @param runner name of the test runner to use
   * @return number of tests run
   */
  synchronized public int runTests(Runner runner, SaveOption save,
      Collection<? extends TestListener> listeners) {

    if (hasCompileProblem()) {
      progress.info("Skipping test run due to compile problem");
      return 0;
    }

    propagateUnknownTestResults();
    clearFlushedStatus();

    progress.updateReflectionNodes();

    long selectionStart = System.currentTimeMillis();

    int[] numTests = { 0, 0 };
    Collection<DependencyNode> testNodes = selectTests(numTests, progress);

    long time = System.currentTimeMillis() - selectionStart;
    Log.info("Test selection took " + time + " millis");

    return runTests(testNodes, runner, save, numTests, listeners);
  }

  /**
   * Run a single test.
   *
   * @param sources
   * @param runner test runner to use
   * @param save save option
   * @param listeners result listeners
   * @return number of tests run
   */
  synchronized public int runSingleTest(Collection<SourceFile> sources, Runner runner,
      SaveOption save, Collection<? extends TestListener> listeners) {

    if (hasCompileProblem()) {
      return 0;
    }

    propagateUnknownTestResults();
    clearFlushedStatus();

    progress.updateReflectionNodes();

    int[] numTests = { 0, 0 };
    Collection<DependencyNode> testNodes = findTestChildren(sources, numTests);
    for (DependencyNode node : testNodes) {
      node.setTestResult(TestResult.UNKNOWN);
    }

    return runTests(testNodes, runner, save, numTests, listeners);
  }

  /**
   * Run the unit tests!
   *
   * @param runner test runner to use
   * @param save save option
   * @param listeners result listeners
   * @return number of tests run
   */
  synchronized public int runAllTests(Runner runner, SaveOption save,
      Collection<? extends TestListener> listeners) {

    if (hasCompileProblem()) {
      return 0;
    }

    propagateUnknownTestResults();
    clearFlushedStatus();

    progress.updateReflectionNodes();

    int[] numTests = { 0, 0 };
    Collection<DependencyNode> testNodes = selectAllTests(numTests, progress);

    return runTests(testNodes, runner, save, numTests, listeners);
  }

  private int runTests(Collection<DependencyNode> testNodes, Runner runner,
      SaveOption save, int[] numTests, Collection<? extends TestListener> listeners) {

    if (testNodes.isEmpty()) {
      progress.info("No tests needed to be run out of " + numTests[0] + " available tests");

      if (DependencyViewerOptions.getIncrementalSave(save)) {
        // Perform incremental map save.
        saveProject();
      }
      return 0;
    }

    try {
      for (TestListener listener : listeners) {
        listener.testRunStarting(numTests[0], numTests[1]);
      }

      // TODO do we have to do this? Don't think we have to...
      // Reset test results for all tests contained in the selected test nodes
      for (DependencyNode testNode : testNodes) {
        Iterator<DependencyNode> iter = testNode.childIterator();
        testNode.clearTestErrorsRecursive();
        while (iter.hasNext()) {
          DependencyNode child = iter.next();
          child.setTestResult(TestResult.UNKNOWN);
        }
      }

      progress.beforeTestRun();

      TestRunner testRunner = TestRunnerFactory.get(runner);
      for (TestListener listener : listeners) {
        testRunner.addListener(listener);
      }
      testRunner.addListener(new DepmapTestListener(this));
      testRunner.runTests(testNodes, this, numTests[1]);

      propagateTestResults();

      return numTests[1];
    } finally {
      for (TestListener listener: listeners) {
        listener.testRunCompleted();
      }

      if (DependencyViewerOptions.getIncrementalSave(save)) {
        // Perform incremental map save.
        saveProject();
      }
    }
  }

  /**
   * Select tests to be run
   * @param numTests An array which will be assigned the total number of
   * available tests [0], and the number of number of tests pending to run [1]
   * @return A collection of dependency nodes representing the tests that
   * need to be run
   */
  synchronized public Collection<DependencyNode> findTestChildren(Collection<SourceFile> sources,
      int[] numTests) {
    numTests[0] = 0;

    Collection<DependencyNode> testNodes = new LinkedList<DependencyNode>();
    for (SourceFile source: sources) {
      Collection<DependencyNode> tests = source.children(TestFilter.INSTANCE);
      numTests[0] += tests.size();
      testNodes.addAll(tests);
    }

    int selectedTests = numTests[0];

    if (!testNodes.isEmpty()) {
      double percent = 100 * selectedTests / (double) numTests[0];
      progress.info(String.format("Selected %d out of %d tests (%.0f%%)",
          selectedTests, numTests[0], percent));
    }

    numTests[1] = selectedTests;

    return testNodes;
  }

  /**
   * Select tests to be run
   * @param numTests An array which will be assigned the total number of
   * available tests [0], and the number of number of tests pending to run [1]
   * @return A collection of dependency nodes representing the tests that
   * need to be run
   */
  synchronized public Collection<DependencyNode> selectAllTests(int[] numTests,
      ProgressListener progressListener) {

    int selectedTests = 0;
    numTests[0] = 0;

    Collection<DependencyNode> testNodes = new LinkedList<DependencyNode>();
    for (SourceFile file : reversePathMap.values()) {
      if (!file.containsTest()) {
        continue;
      }
      Iterator<DependencyNode> iter = file.childIterator();
      while (iter.hasNext()) {
        DependencyNode testNode = iter.next();
        if (testNode.containsTest()) {
          numTests[0] += Math.max(1, testNode.numChildren());

          testNodes.add(testNode);
          selectedTests += Math.max(1, testNode.numChildren());
        }
      }
    }

    if (!testNodes.isEmpty()) {
      double percent = 100 * selectedTests / (double) numTests[0];
      progressListener.info(String.format("Selected %d out of %d tests (%.0f%%)",
          selectedTests, numTests[0], percent));
    }

    numTests[1] = selectedTests;

    return testNodes;
  }

  /**
   * Select tests to be run
   * @param numTests An array which will be assigned the total number of
   * available tests [0], and the number of number of tests pending to run [1]
   * @return A collection of dependency nodes representing the tests that
   * need to be run
   */
  synchronized public Collection<DependencyNode> selectTests(int[] numTests,
      ProgressListener progressListener) {

    int selectedTests = 0;
    numTests[0] = 0;

    Collection<DependencyNode> testNodes = new LinkedList<DependencyNode>();
    for (SourceFile file : reversePathMap.values()) {
      if (!file.containsTest()) {
        continue;
      }
      Iterator<DependencyNode> iter = file.childIterator();
      while (iter.hasNext()) {
        DependencyNode testNode = iter.next();
        if (testNode.containsTest()) {
          numTests[0] += Math.max(1, testNode.numChildren());

          if (testNode.mustRunTest()) {

            testNodes.add(testNode);
            Log.info("Selected test: " + testNode.fullName());
            selectedTests += Math.max(1, testNode.numChildren());
          }
        }
      }
    }

    if (!testNodes.isEmpty()) {
      double percent = 100 * selectedTests / (double) numTests[0];
      progressListener.info(String.format("Selected %d out of %d tests (%.0f%%)",
          selectedTests, numTests[0], percent));
    }

    numTests[1] = selectedTests;

    return testNodes;
  }

  /**
   * Propagate test results after test run
   */
  synchronized public void propagateTestResults() {

    for (SourceFile file : reversePathMap.values()) {
      if (!file.containsTest()) {
        continue;
      }
      Iterator<DependencyNode> iter = file.childIterator();
      while (iter.hasNext()) {
        DependencyNode node = iter.next();
        if (node.isTest() && node.numChildren() > 0) {
          boolean allPassed = true;
          for (DependencyNode child: node) {
            if (child.getTestResult() != TestResult.PASSED) {
              allPassed = false;
              if (child.getTestResult() == TestResult.FAILED) {
                node.setTestResultNonRecursive(TestResult.FAILED);
              } else if (child.getTestResult() == TestResult.UNKNOWN) {
                node.setTestResultNonRecursive(TestResult.UNKNOWN);
              }
              break;
            }
          }
          if (allPassed) {
            node.setTestResultNonRecursive(TestResult.PASSED);
          }
        }
      }
    }
  }

  /**
   * Open and initialize a dependency project
   * @param newProject
   */
  public synchronized void openProject(Project newProject) {
    this.project = newProject;

    // Always clear the classpath and reset the dependency map
    // when loading a new project.
    clearClassPath();
    resetDependencyMap();

    progress.projectLoaded(newProject);

    if (!project.hasConfigFile() || !loadProject()) {
      if (project.getClasspathFile().isFile()) {
        loadClasspath();
        saveProject();
        scanSourceFiles();
      } else {
        progress.projectConfigMissing();
      }
    }
  }

  /**
   * @return The current dependency project
   */
  public Project getProject() {
    return project;
  }

  /**
   * Scan for source file changes in the filesystem
   */
  public void scanSourceFiles() {
    Thread job = new Thread() {
      @Override
      public void run() {
        scanSourceFilesBlocking();
      }
    };
    job.start();
  }

  /**
   * Scan for source file changes in the filesystem
   */
  public void scanSourceFilesBlocking() {
    fileSystemPoller.synchronizeWithFileSystem();
  }

  /**
   * @return <code>true</code> if the dependency map needs to be refreshed
   */
  public boolean needsRefresh() {
    return needsRefresh;
  }

  /**
   * Notify the dependency map that it should be refreshed
   */
  public void setNeedsRefresh() {
    needsRefresh = true;
  }

  public void forceDependencyRefresh() {
  }

  /**
   * Print some statistics about the dependency map.
   */
  public void printStats() {
    System.out.println("num files: " + reversePathMap.size());
    System.out.println("num nodes: " + map.size());
  }

  /**
   * Compile test source files.
   * @return zero for success, non-zero for failure
   */
  public int compileSources() {
    List<String> command = new LinkedList<String>();
    command.add(project.getJavaCompiler());

    StringBuilder classpath = new StringBuilder();
    for (File jarFile : getJarLibraries()) {
      if (classpath.length() > 0) {
        classpath.append(File.pathSeparatorChar);
      }
      classpath.append(jarFile.getPath());
    }
    if (classpath.length() > 0) {
      command.add("-classpath");
      command.add(classpath.toString());
    }
    // Add extra flags.
    for (String flag: project.getJavacFlags()) {
      command.add(flag);
    }
    command.add("-d");
    File tempDir = createOutputDir();
    command.add(tempDir.getPath());
    for (File sourceFile : getSourceFiles()) {
      command.add(sourceFile.getPath());
    }

    ProcessBuilder builder = new ProcessBuilder(command);
    try {
      // Debug command printing.
      StringBuilder sb = new StringBuilder();
      sb.append("compile command: ");
      for (String part: command) {
        sb.append(part);
        sb.append(" ");
      }
      Log.info(sb.toString());
      ByteArrayOutputStream errBuf = new ByteArrayOutputStream();
      byte[] buf = new byte[256];
      long start = System.currentTimeMillis();
      Process process = builder.start();
      InputStream errStream = process.getInputStream();
      int read = 0;
      do {
        read = errStream.read(buf);
        if (read > 0) {
          errBuf.write(buf, 0, read);
        }
      } while (read != -1);
      errStream.close();

      // Read errors.
      int exitCode = process.waitFor();
      long time = System.currentTimeMillis() - start;
      Log.info("javac compilation took " + time + " millis");
      if (exitCode != 0) {
        System.out.println(errBuf.toString());
      }
      return exitCode;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return 1;
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return 2;
    }
  }

  public void printClasspath() {
    System.out.println("classpath:");
    for (File jar: getJarLibraries()) {
      System.out.println(jar.getPath());
    }
  }

}
