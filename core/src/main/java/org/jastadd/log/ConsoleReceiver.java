/* Copyright (c) 2014 Jesper Öqvist <jesper@llbit.se>
 * All rights reserved.
 */
package org.jastadd.log;

public class ConsoleReceiver extends Receiver {

	public static final ConsoleReceiver INSTANCE = new ConsoleReceiver();

	private ConsoleReceiver() {
	}

	@Override
	public void logEvent(Level level, String message) {
		switch (level) {
		case INFO:
		case WARNING:
			System.out.println(message);
			break;
		case ERROR:
			System.err.println(message);
			break;
		}
	}

}
