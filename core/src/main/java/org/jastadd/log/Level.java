/* Copyright (c) 2014 Jesper Öqvist <jesper@llbit.se>
 * All rights reserved.
 */
package org.jastadd.log;

public enum Level {
	ERROR,
	WARNING,
	INFO
}
