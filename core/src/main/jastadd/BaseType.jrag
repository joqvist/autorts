/* vim: set noet ts=4 sw=4: */

/**
 * Base type utility attribute.
 */
aspect BaseType {

	/**
 	 * Utility attribute to get the base type of raw and generic
 	 * TypeDecls.
 	 */
	syn TypeDecl Expr.baseType() = type().baseType();

	/**
 	 * Utility attribute to get the base type of raw, generic
 	 * or array TypeDecls.
 	 */
	syn TypeDecl TypeDecl.baseType() = this;
	eq ArrayDecl.baseType() = elementType();
	eq ParClassDecl.baseType() = original();
	eq ParInterfaceDecl.baseType() = original();
	eq RawClassDecl.baseType() = original();
	eq RawInterfaceDecl.baseType() = original();
	eq BoundTypeAccess.baseType() = getTypeDecl().baseType();
	eq ClassDeclSubstituted.baseType() = getOriginal();
	eq InterfaceDeclSubstituted.baseType() = getOriginal();
	eq GenericClassDeclSubstituted.baseType() = getOriginal();
	eq GenericInterfaceDeclSubstituted.baseType() = getOriginal();
}
