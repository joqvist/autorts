package org.jastadd.testsel.testapi.external;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jastadd.testsel.meta.TestResult;
import org.jastadd.testsel.testapi.ExternalTestRunner;
import org.jastadd.testsel.testapi.TestListener;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

/**
 * JUnit 4 test runner.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JUnit4ExternalTestRunner extends RunListener implements ExternalTestRunner {

	private final Collection<TestListener> listeners = new LinkedList<TestListener>();

	@Override
	public boolean runTests(Collection<String> tests) {
		boolean classesLoaded = true;

		List<Class<?>> classList = new ArrayList<Class<?>>();
		for (String className : tests) {
			// load the test class
			try {
				classList.add(Class.forName(className));
			} catch (ClassNotFoundException e) {
				onClassNotFound(className);
				classesLoaded = false;
			}
		}

		if (!classesLoaded) {
			return false;
		}

		Class<?>[] classes = new Class[classList.size()];
		for (int i = 0; i < classList.size(); ++i) {
			classes[i] = classList.get(i);
		}

		// silence stdout and stderr
		final PrintStream stdOut = System.out;
		final PrintStream stdErr = System.err;

		Result juResult;

		try {
			System.setOut(new PrintStream(new ByteArrayOutputStream()));
			System.setErr(new PrintStream(new ByteArrayOutputStream()));

			JUnitCore junit = new JUnitCore();

			// register a listener for the test results
			junit.addListener(this);

			// run the tests
			juResult = junit.run(classes);

		} finally {
			// restore standard out and standard err
			System.setOut(stdOut);
			System.setErr(stdErr);
		}

		return juResult.wasSuccessful();
	}

	private void onClassNotFound(String className) {
		// TODO inform listeners of class loading problem
		for (TestListener listener: listeners) {
			listener.testError(className, "class not found");
		}
	}

	@Override
	public void addListener(TestListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(TestListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void testFailure(Failure failure) throws Exception {
		System.out.println(failure.toString());
		Description description = failure.getDescription();
		//listener.testFinished(qualifiedName(description), TestResult.FAILED);
		String testName = qualifiedName(description);
		for (TestListener listener: listeners) {
			listener.testError(testName, failure.getException());
		}
	}

	@Override
	public synchronized void testFinished(Description description) throws Exception {
		String testName = qualifiedName(description);
		for (TestListener listener: listeners) {
			listener.testFinished(testName, TestResult.PASSED);
		}
	}

	@Override
	public void testStarted(Description description) throws Exception {
		String testName = qualifiedName(description);
		for (TestListener listener: listeners) {
			listener.testStarted(testName);
		}
	}

	private String qualifiedName(Description description) {
		return description.getClassName() + "." +
				stripParameters(description.getMethodName()) + "()";
	}

	private static final Pattern parameterPattern = Pattern.compile("(\\w+)(\\[.*\\])?");

	/**
	 * Strip parameterized test parameters from the method name
	 * @param methodName
	 * @return stripped version of method name
	 */
	private String stripParameters(String methodName) {
		Matcher matcher = parameterPattern.matcher(methodName);
		if (matcher.matches()) {
			return matcher.group(1);
		} else {
			return methodName;
		}
	}
}
