package org.jastadd.testsel.testapi.external;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import junit.framework.AssertionFailedError;
import junit.framework.Test;
import junit.framework.TestSuite;
import junit.runner.Version;

import org.jastadd.testsel.meta.TestResult;
import org.jastadd.testsel.testapi.ExternalTestRunner;
import org.jastadd.testsel.testapi.TestListener;

/**
 * JUnit 3 test runner.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JUnit3ExternalTestRunner implements ExternalTestRunner, junit.framework.TestListener {

	private final Collection<TestListener> listeners = new LinkedList<TestListener>();

	@Override
	public boolean runTests(Collection<String> tests) {
		boolean classesLoaded = true;

		List<Class<?>> classList = new ArrayList<Class<?>>();
		for (String className : tests) {
			// load the test class
			try {
				classList.add(Class.forName(className));
			} catch (ClassNotFoundException e) {
				onTestException(className, e);
				classesLoaded = false;
			}
		}

		if (!classesLoaded) {
			return false;
		}

		Class<?>[] classes = new Class[classList.size()];
		for (int i = 0; i < classList.size(); ++i) {
			classes[i] = classList.get(i);
		}

		// silence stdout and stderr
		final PrintStream stdOut = System.out;
		final PrintStream stdErr = System.err;

		boolean successful = true;

		System.out.println("Starting test run with JUnit " + Version.id());

		try {
			//PrintStream out = new PrintStream(new ByteArrayOutputStream());
			//PrintStream err = new PrintStream(new ByteArrayOutputStream());
			//System.setOut(out);
			//System.setErr(err);

			junit.framework.TestResult testResult = new junit.framework.TestResult();
			testResult.addListener(this);
			for (Class<?> klass: classList) {
				String className = klass.getName();
				try {
					Test test = new TestSuite(klass);
					test.run(testResult);
				} catch (Throwable thrown) {
					onTestException(className, thrown);
				}
			}

			//out.close();
			//err.close();

		} finally {
			// restore standard out and standard err
			System.setOut(stdOut);
			System.setErr(stdErr);
		}

		return successful;
	}

	private void onTestException(String className, Throwable thrown) {
		for (TestListener listener: listeners) {
			listener.testError(className, thrown);
		}
	}

	@Override
	public void addListener(TestListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(TestListener listener) {
		listeners.remove(listener);
	}

	private String qualifiedName(Test test) {
		return test.getClass().getName();
	}

	@Override
	public void addError(Test test, Throwable thrown) {
		System.out.println(thrown.toString());
		String testName = qualifiedName(test);
		for (TestListener listener: listeners) {
			listener.testError(testName, thrown);
		}
	}

	@Override
	public void addFailure(Test test, AssertionFailedError failure) {
		System.out.println(failure.toString());
		//failure.printStackTrace();
		String testName = qualifiedName(test);
		for (TestListener listener: listeners) {
			listener.testError(testName, failure);
		}
	}

	@Override
	public void endTest(Test test) {
		String testName = qualifiedName(test);
		for (TestListener listener: listeners) {
			listener.testFinished(testName, TestResult.PASSED);
		}
	}

	@Override
	public void startTest(Test test) {
		String testName = qualifiedName(test);
		for (TestListener listener: listeners) {
			listener.testStarted(testName);
		}
	}
}
